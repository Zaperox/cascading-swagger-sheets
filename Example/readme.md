# What's the deal with .cswgs? #
.cswgs is a format by Zaperox which allows for PHP to provide CSS.

# What can .cswgs do for me? #
* It can be used to reduce browser requests and inprove load times!
* It can work around some ad-blockers!
* It can allow you to use http method "GET" to change the outcome of the PHP!
* It can make your website cooler, like changing the color of the header dependent on the time of day!
*You can make CSS change via PHP without having to add <style> tags to every page, or even worse, having to add ugly inline CSS styles!